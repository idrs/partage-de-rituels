from masonite.controllers import Controller
from masonite.views import View
from app.models.Question import Question
from masonite.request import Request
from masonite.response import Response


import subprocess
import unicodedata
import re
import os
import base64



def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')

class QuestionController(Controller):
    def show(self, view: View):
        questions = Question.all()
        return view.render("question",{"questions":questions})

    def single(self, view: View, request: Request):
        question = Question.find(request.param('id'))

        return view.render('editquestion', {'question': question})

    def store(self, view: View, request: Request, response: Response):
        question = Question.create(
            title=request.input('title'),
            body="",
            tags="",
            competences="",
            attachments="",
            accessed=1,
            author_id=request.user().id
        )

        return response.redirect(f'/question/{question.id}')
    
    def update(self, view: View, request: Request, response: Response):
        
        basePath = "storage/compiled"
        
        question = Question.find(request.param('id'))

        question.title = request.input('title')
        question.body = request.input('body')
        question.tags = request.input('tags')

        question.save()

        cleaned_name = slugify(question.title)

        preambule = '#set page(\n'\
            'paper: "presentation-4-3",\n'\
            'margin: 0pt,\n'\
            ')\n'

        path = f"{basePath}/q/{question.id}/"
        
        if not os.path.exists(path):
            os.makedirs(path)
   
        with open(f"{basePath}/q/{question.id}/{cleaned_name}.typ", "w") as file1:
            # Writing data to a file
            file1.write(preambule+question.body)

        result = subprocess.run(['typst', 'compile',f"{basePath}/q/{question.id}/{cleaned_name}.typ",f"{basePath}/q/{question.id}/{cleaned_name}.pdf"], capture_output=True, text=True)
        stdout = result.stdout
        stderr = result.stderr
        print("Sortie typst:")
        print(stdout,stderr)

        with open(f"storage/public/q/{question.id}/{cleaned_name}.pdf", "rb") as pdf_file:
            encodedpdf = base64.b64encode(pdf_file.read())
        
        return {"rep":stdout,"error":stderr,"bdata":encodedpdf.decode('utf-8'),"url":f"/q/{question.id}/{cleaned_name}.pdf"}

# quelque chose à ajouter pour le fun