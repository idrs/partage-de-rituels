""" Question Model """

from masoniteorm.models import Model
from masoniteorm.relationships import belongs_to

class Question(Model):
    """Question Model"""
    #__table__ = 'questions'
    __fillable__ = ['title', 'author_id', 'body','tags','competences','attachments','accessed']
    
    @belongs_to('author_id', 'id')
    def author(self):
        from app.models.User import User
        return User
