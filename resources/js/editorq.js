
//import "./bootstrap.js";
//import * as pdfjsLib from "./pdf.js";

var pdfjsLib = window['pdfjs-dist/build/pdf'];
//pdfjsLib.GlobalWorkerOptions.workerSrc = '/static/js/pdf.worker.js';


pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/3.8.162/pdf.worker.min.js';

  

  function loadPdf(data){ 
    console.log("loadPDF with b64")
    var thePdf = null;
    var scale = 1;
    
    var pdfData = atob(data);

    var viewerWidth = document.getElementById('viewer').offsetWidth;

    function renderPage(pageNumber, canvas) {
        thePdf.getPage(pageNumber).then(function(page) {
          scale = (viewerWidth-4)/page.view[2];
            viewport = page.getViewport({ scale: scale });
          canvas.height = viewport.height;
          canvas.width = viewport.width;          
          page.render({canvasContext: canvas.getContext('2d'), viewport: viewport});
    });
    }

    pdfjsLib.getDocument({data: pdfData}).promise.then(function(pdf) {
        thePdf = pdf;
        viewer = document.getElementById('viewer');
        
        for(page = 1; page <= pdf.numPages; page++) {
          var idname = 'pdf-p'+page;
          var canvas = document.getElementById(idname);
          if(!canvas){
            canvas = document.createElement("canvas");    
            canvas.className = 'pdf-page-canvas';
            canvas.id = 'pdf-p'+page;         
            viewer.appendChild(canvas);
          }                      
          renderPage(page, canvas);
        }
    });
    
    
}
// maintain out of the scope of the event
var to;

var editArea = document.getElementById("body")

editArea.addEventListener('keyup',function(event){
  // if it exists, clear it and prevent it from occuring
  if (to) clearTimeout(to);

  // reassign it a new timeout that will expire (assuming user doesn't
  // type before it's timed out)
  to = setTimeout(function(){
    handleUpdate();
  }, 3e3 /* 3 seconds or whatever */);
});


//"/question/{{question.id}}"


async function handleUpdate(event) {
	/**
	 * This prevents the default behaviour of the browser submitting
	 * the form so that we can handle things instead.
	 */
	const form = editArea.parentNode;
	/**
	 * This gets the element which the event handler was attached to.
	 *
	 * @see https://developer.mozilla.org/en-US/docs/Web/API/Event/currentTarget
	 */
	//const form = event.currentTarget;

	/**
	 * This takes the API URL from the form's `action` attribute.
	 */
	const url = form.action;

	
		/**
		 * This takes all the fields in the form and makes their values
		 * available through a `FormData` instance.
		 * 
		 * @see https://developer.mozilla.org/en-US/docs/Web/API/FormData
		 */
		const formData = new FormData(form);

		/**
		 * We'll define the `postFormDataAsJson()` function in the next step.
		 */
		putFormDataAsJson({ url, formData })
        .then(data =>  {
			console.log("out: ",data.rep)
			console.log("error: ",data.error)
			
			loadPdf(data.bdata)
		}
         )
        .catch(err => console.log('Error',err))
		/**
		 * Normally you'd want to do something with the response data,
		 * but for this example we'll just log it to the console.
		 */

}

    const editorForm = document.getElementById("editor");

/**
 * We'll define the `handleFormSubmit()` event handler function in the next step.
 */
    editorForm.addEventListener("submit", handleFormSubmit);

    /**
 * Event handler for a form submit event.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/submit_event
 * 
 * @param {SubmitEvent} event
 */
async function handleFormSubmit(event) {
	/**
	 * This prevents the default behaviour of the browser submitting
	 * the form so that we can handle things instead.
	 */
	event.preventDefault();

	/**
	 * This gets the element which the event handler was attached to.
	 *
	 * @see https://developer.mozilla.org/en-US/docs/Web/API/Event/currentTarget
	 */
	const form = event.currentTarget;

	/**
	 * This takes the API URL from the form's `action` attribute.
	 */
	const url = form.action;

	
		/**
		 * This takes all the fields in the form and makes their values
		 * available through a `FormData` instance.
		 * 
		 * @see https://developer.mozilla.org/en-US/docs/Web/API/FormData
		 */
		const formData = new FormData(form);

		/**
		 * We'll define the `postFormDataAsJson()` function in the next step.
		 */
		putFormDataAsJson({ url, formData })
        .then(data => {
            console.log('stdout:',data.rep)
            loadPdf(data.bdata);
        } 
         )
        .catch(err => console.log('Error',err))
		/**
		 * Normally you'd want to do something with the response data,
		 * but for this example we'll just log it to the console.
		 */

}

/**
 * Helper function for POSTing data as JSON with fetch.
 *
 * @param {Object} options
 * @param {string} options.url - URL to POST data to
 * @param {FormData} options.formData - `FormData` instance
 * @return {Object} - Response body from URL that was POSTed to
 */
 async function putFormDataAsJson({ url, formData }) {
	/**
	 * We can't pass the `FormData` instance directly to `fetch`
	 * as that will cause it to automatically format the request
	 * body as "multipart" and set the `Content-Type` request header
	 * to `multipart/form-data`. We want to send the request body
	 * as JSON, so we're converting it to a plain object and then
	 * into a JSON string.
	 * 
	 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
	 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/fromEntries
	 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify
	 */
	const plainFormData = Object.fromEntries(formData.entries());
	const formDataJsonString = JSON.stringify(plainFormData);

	const fetchOptions = {
		/**
		 * The default method for a request with fetch is GET,
		 * so we must tell it to use the POST HTTP method.
		 */
		method: "PUT",
		/**
		 * These headers will be added to the request and tell
		 * the API that the request body is JSON and that we can
		 * accept JSON responses.
		 */
		headers: {
			"Content-Type": "application/json",
			"Accept": "application/json"
		},
		/**
		 * The body of our POST request is the JSON string that
		 * we created above.
		 */
		body: formDataJsonString,
	};

	const response = await fetch(url, fetchOptions);

	if (!response.ok) {
		const errorMessage = await response.text();
		throw new Error(errorMessage);
	}

	return response.json();
}
