from masonite.routes import Route
from masonite.authentication import Auth

ROUTES = [
    Route.get("/", "WelcomeController@show"),
    
      # Routes de Question
    Route.get('/question', 'QuestionController@show'),

    Route.post('/question','QuestionController@store'),
    Route.get('/question/@id','QuestionController@single'),
    Route.put('/question/@id','QuestionController@update')
]

ROUTES += Auth.routes()