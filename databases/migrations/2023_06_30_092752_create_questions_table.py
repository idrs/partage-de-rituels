"""CreateQuestionsTable Migration."""

from masoniteorm.migrations import Migration


class CreateQuestionsTable(Migration):
    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create("questions") as table:
            table.increments("id")
            table.string("title")
            
            table.integer('author_id').unsigned()
            table.foreign('author_id').references('id').on('users')

            table.string('body')
            table.string('tags')
            table.string('competences')
            
            table.string('attachments')
            table.integer('accessed')

            table.timestamps()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop("questions")
